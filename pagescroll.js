
'Use strict'

window.requestAnimationFrame = requestAnimationFrame;

const menu = document.querySelector('.nav-menu');
const items = menu.querySelectorAll('.nav-menu__item');
const containers = document.querySelectorAll('.nav-menu-anchor-js');
const pageHeight = document.body.scrollHeight;
const headerHeight = 72;


menu.onclick = function (e) {
	if (e.target.tagName != 'SPAN') return;
	let current = switchLinks(e.target);
	selectContainer(current);
}

function switchLinks(el) {
	let current;
	[].forEach.call(items, function (item, index) {
		item.classList.remove('nav-menu__item_active');
		if (item === el) {
			item.classList.add('nav-menu__item_active');
			current = index;
		}
	});
	return current;
}

function selectContainer(current) {
	[].forEach.call(containers, function (container, index) {
		if (index == current) {
			let startY = container.getBoundingClientRect().top - headerHeight;
			let direction = (startY < 0) ? -1 : (startY > 0) ? 1 : 0;
			if (direction == 0) return;
			scroll(container, direction);
		}
	});
}

function scroll(el, direction) {
	const duration = 1500;
	const start = new Date().getTime();

	let fn = function () {
		let top = el.getBoundingClientRect().top - headerHeight;
		let now = new Date().getTime() - start;
		let result = Math.round(top * now / duration);

		result = (result > direction * top) ? top : (result == 0) ? direction : result;

		if (direction * top > 0 && (pageHeight - window.pageYOffset) > direction * document.documentElement.clientHeight) {
			window.scrollBy(0, result);
			requestAnimationFrame(fn);
		}
	}

	requestAnimationFrame(fn);
}
